let url = "https://lobinhos.herokuapp.com/wolves/"

// Gera o HTML
function showWolf(lobo) {
    const main = document.getElementById("main")
    const container = document.createElement("section")
    container.id = "container"

    container.innerHTML = `
    <section class="lista-lobos" id="lobos">
        <h1 id="wolfshowtitle" class = "marginwolfshow">${lobo.name}</h1>
        <div id ="divsectionnotitle">
            <div id = "divimgbuttons">
                <div>
                    <figure><img class="fotolobo"src="${lobo.link_image}"></figure>
                </div>
                <div id="divbuttons">
                    <button type="button" id="buttonadopt">ADOTAR</button>
                    <button type="button" id="buttonerase">EXCLUIR</button>
                </div>
            </div>
            <p id="paragrafo">${lobo.description}</p>
        </div>
    </section>
    `
    container.querySelector("#buttonadopt").addEventListener("click", (event) => {
        window.location = `adoptwolf.html?id=${idAtual}`
    })

    container.querySelector("#buttonerase").addEventListener("click", (event) => {
        const fetchConfig = {
            method: "DELETE"
        }
        fetch(url, fetchConfig)
        .then(function() {
            alert("Lobo deletado com sucesso!")
            window.location = "wolves.html"})
        .catch(error => console.warn(error))
    })

    main.appendChild(container)
}

// Pega o ID da URL
let idAtual = window.location.href
idAtual = idAtual.slice(idAtual.lastIndexOf('=') + 1, idAtual.length)
url += idAtual

// Carrega o lobo com o ID da URL
fetch(url)
.then(resp => resp.json())
.then(lobo => {showWolf(lobo.wolf)})

