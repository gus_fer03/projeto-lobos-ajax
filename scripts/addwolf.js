const url = "https://lobinhos.herokuapp.com/wolves"

function salvarLobo() {
    const nome = document.getElementById("nome").value
    const idade = document.getElementById("anos").value
    const fotoLink = document.getElementById("foto-link").value
    const desc = document.getElementById("descricao").value

    body = {"wolf":{
        "name": nome,
        "age": idade,
        "link_image": fotoLink,
        "description": desc}
    }

    let fetchConfig = {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(body)
    }

    fetch(url, fetchConfig)
    .then(resp => resp.json())
    .then(lobo => {
        console.log(lobo)
    })
    .then(alert("Lobo adicionado com sucesso!"))
    .catch(error => console.warn(error))
}