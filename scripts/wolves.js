let url = "https://lobinhos.herokuapp.com/wolves"
import {transformDiv} from "./modules.js"

// Cria o card de adotados
function mostrarAdotados(lobo) {
  fetch("https://lobinhos.herokuapp.com/wolves/" + lobo.id)
  .then(resp => resp.json())
  .then(loboAdotado => {
    loboAdotado = loboAdotado.wolf_adopted.adoption
    const lobos = document.querySelector(".lista-lobos")
    const li = document.createElement("li")
    li.id = lobo.id
    li.className = "bloco-lobo"

    console.log(loboAdotado)
    console.log(loboAdotado)
    li.innerHTML = `
        <div class="container" style="background-image: url(${lobo.link_image});"></div>
        <div class="lobo-texto">
            <h3 class="lobo-name">${lobo.name}</h3>
            <p class="idade" id="sobre-lobo-1">Idade: ${lobo.age} anos</p>
            <p class="sobre-lobo-1">${lobo.description}</p>
            <br>
            <p class="lobo-dono">Adotado por: ${loboAdotado.name}</p>
        </div>
        <div class="container-botao"><button class="botao-adotar" id="botao-adotar-${lobo.id}" style="background-color: #7AAC3A">Adotado</button></div>
        `
    lobos.append(li)
  })
}

// Pegar os lobos ao carregar a página
fetch(url)
.then(resp => resp.json())
.then(lobos => {for (const lobo of lobos.wolves) {transformDiv(lobo)}})

// Checkbox
const checkboxFiltro = document.getElementById("filtro")

checkboxFiltro.addEventListener("click", (elemento) => {
    if(checkboxFiltro.checked){
        document.querySelector(".lista-lobos").innerHTML = ""
        url = "https://lobinhos.herokuapp.com/wolves/adopted"
        fetch(url)
        .then(resp => resp.json())
        .then(lobos => {for (const lobo of lobos.wolves) {mostrarAdotados(lobo)}})
    } else {
        document.querySelector(".lista-lobos").innerHTML = ""
        url = "https://lobinhos.herokuapp.com/wolves"
        fetch(url)
        .then(resp => resp.json())
        .then(lobos => {for (const lobo of lobos.wolves) {transformDiv(lobo)}})}
})

// Filtro de busca
const searchbar = document.getElementById("barra-pesquisa")
searchbar.addEventListener("change", (event) => { 
  for(const lobo of document.getElementsByClassName("bloco-lobo")) {
    if (!(lobo.querySelector(".lobo-name").innerText.includes(event.target.value))){
      console.log(lobo)
      lobo.remove()
    }
  }
});

// Paginação
let lobos = document.querySelector(".lista-lobos");

function paginationnumbers(totaldepaginas){
  const ul = document.getElementById('listanumpaginacao')
  for(let i=0; i<totaldepaginas; i++){
    let li = document.createElement('li')
    li.innerHTML = `<a href="#">${i+1}</a>`
    ul.appendChild(li)
    }
    let li = document.createElement('li')
    li.innerHTML = `<a href="#">»</a>`
    ul.appendChild(li)
}


