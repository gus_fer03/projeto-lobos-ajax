// Transforma lobo em card
function transformDiv(lobo) {
    const lobos = document.querySelector(".lista-lobos")
    const li = document.createElement("li")
    li.id = lobo.id
    li.className = "bloco-lobo"

    li.innerHTML = (document.title == "Nossos Lobinhos" && !(window.location.href.includes("?"))) ? 
    `
        <div class="container" style="background-image: url(${lobo.link_image});"></div>
        <div class="lobo-texto">
            <h3 class="lobo-name">${lobo.name}</h3>
            <p class="idade" id="sobre-lobo-1">Idade: ${lobo.age} anos</p>
            <p class="sobre-lobo-1">${lobo.description}</p> 
        </div>
        <div class="container-botao"><button class="botao-adotar" id="botao-adotar-${lobo.id}">Adotar</button></div>
        ` :
        `
        <div class="container" style="background-image: url(${lobo.link_image});"></div>
        <div class="lobo-texto">
            <h3 class="lobo-name">${lobo.name}</h3>
            <p id="lobo-name" id="sobre-lobo-1">Idade: ${lobo.age} anos</p>
            <p class="sobre-lobo-1">${lobo.description}</p> 
        </div>` 
        
    // Verifica se a página é "Nossos Lobinhos"
    if (document.title == "Nossos Lobinhos" && !(window.location.href.includes("?"))){
        // Caso seja, é adicionado um listener ao botão Adotar
        li.querySelector(`#botao-adotar-${lobo.id}`).addEventListener("click", (event) => {
            window.location = `showwolf.html?id=${event.target.parentNode.parentNode.id}`
        })
    }

    lobos.appendChild(li)
}

export {transformDiv};