let url = "https://lobinhos.herokuapp.com/wolves"
import {transformDiv} from "./modules.js"

fetch(url)
.then(resp => resp.json())
.then(lobos => {
    transformDiv(lobos.wolves[0])
    transformDiv(lobos.wolves[1])})