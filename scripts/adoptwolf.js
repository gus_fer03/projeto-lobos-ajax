let url = "https://lobinhos.herokuapp.com/wolves/"

// Gera o HTML
function adoptWolf(lobo) {
    const form = document.getElementById("flexadoptphoto")
    const div = document.createElement("div")

    div.innerHTML = `
    <div class='flexadoptphoto'>
        <div class='fotoredonda'>
            <img src ="${lobo.link_image}"/>
            </div>
        
        <div class="flexadopttitle">
            <h1 id="adoptwolfnameformtitle" class = "marginadopttitle">Adote o(a) ${lobo.name}</h1>
            <sub class= "marginadopttitle" id="subidwolf">ID: ${lobo.id}</sub>
        </div>
        </div>
        </div>
        
        `

    // Adiciona o request ao botao
    document.querySelector("#adotar").addEventListener("click", (event) => {
        // Junta as informaçoes pro body
        const urlAdopted = "https://lobinhos.herokuapp.com/wolves/adoption"
        const nome = document.getElementById("nome").value
        const idade = document.getElementById("idade").value
        const email = document.getElementById("email").value
        const id = lobo.id

        let body = {
            "adoption":{
                "name": nome,
                "age": idade,
                "email": email,
                "wolf_id": id}
            }

        // Configura o request
        let fetchConfig = {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(body)
        }

        // Faz o request
        fetch(urlAdopted, fetchConfig)
        .then(resp => resp.json())
        .then(lobo => console.log(lobo))
        .then(alert("Lobo adotado com sucesso!"))
        .then(window.location = "wolves.html")
    })
    form.appendChild(div)
}

// Pega o ID da URL
let idAtual = window.location.href
idAtual = idAtual.slice(idAtual.lastIndexOf('=') + 1, idAtual.length)
url += idAtual

// Carrega o lobo com o ID da URL
fetch(url)
.then(resp => resp.json())
.then(lobo => {adoptWolf(lobo.wolf)})